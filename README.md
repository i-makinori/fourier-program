# Fourier Program

Fourie analysis Program

## Useage

### FFT

```commonlisp
;; define wave-series,
(defparameter *exam-series-square-32*
  (map 'vector (square-wave 4 1.37) (series-of-range-interval-samples 0 4 32)))
(defparameter *exam-series-delta-256*
  (map 'vector (delta-wave 2 0.95) (series-of-range-interval-samples 0 4 256)))
(defparameter *exam-series-ecos23-400* ;; 400 is not 2^n exam
  (map 'vector #'(lambda (x) (expt (cos x) 23)) (series-of-range-interval-samples 0 10 400)))

;; to fft,
(fft *exam-series-ecos23-400*)

;; to dft,
(dft *exam-series-ecos23-400*)

;; to ifft,
(ifft (dft *exam-series-ecos23-400*)) ;; or also, fft applied spectrum, you can use.

;; plotting series for example DFT, IFFT, FFT ed wave-series
(plot-series *exam-series-ecos23-400*)
(plot-series (ifft (dft *exam-series-ecos23-400*)))
(plot-series (fft *exam-series-ecos23-400*))
(plot-series (ifft (fft *exam-series-ecos23-400*)))
(plot-series-es (list (ifft (fft *exam-series-square-32*)) *exam-series-square-4096*))
;; to do to better plotting

;; plotting spectrum
(plot-spectrum (power-spectre-gen) (fft *exam-series-delta-256*))
;; select spectrum-gen function and its parameter to select spectrum type.
;; such as energy, power, ...

```

### Analyse Music

```commonlisp

;; register wav file
;; copy your wav file into ./data/sound_file_xxxx.wav
;; after that, run 
(re-register-wav-file-name-list!) ;; to reset sound file list
*wav-file-name-list* ;; to check list of sound files.
;; to do convertion, 
;; $ ffmpeg -i input.mp3 output.wav # or 
;; $ mpg123 -w output.wav input.mp3 # are usable in linux.

;; spectrogram of specific (numbered) wav file.
(nth 13 *wav-file-name-list*) ;; -> #P"/some/sound/great/wav-file.wav" ;; please change 13 into number of file for analyse.
(plot-wave-data (read-wav-file-into-wave-data (nth 13 *wav-file-name-list*)) 10000)
(draw-wave-file-spectrogram (nth 13 *wav-file-name-list*) *spectrogram-filename* 2048 128)

;; diff spectrogram

(draw-wave-file-spectrogram (nth 1 *wav-file-name-list*) *spectrogram-filename* 2048 64) ;; spectrogram
(draw-wave-file-diff-spectrogram (nth 1 *wav-file-name-list*) *spectrogram-filename* 2048 64 #'diff-of-horizontal) ;; diff map of amplitude at each requency
(draw-wave-file-diff-spectrogram (nth 1 *wav-file-name-list*) *spectrogram-filename* 2048 64 #'diff-of-vertical) ;; diff map of frequency at each time
(draw-wave-file-diff-spectrogram (nth 1 *wav-file-name-list*) *spectrogram-filename* 2048 64 #'diff-of-vertical) ;; diff map of frequency at each time of both
```


## Todo

### 一般周波数解析

区間[0, T_Max]に於ける、スペクトラムの図示。


### 周波数の時系列解析。

区間[t, t+Δt]に毎に於ける、スペクトラムの図示。


### 振動モードの吸収

ドレミファ解析


## アルゴリズム

### フーリエ解析

- [x] 高速フーリエ変換 (FFT)
- [x] 高速逆フーリエ変換 (IFFT)
- [x] 結果のプロット
- [ ] 見やすいプロット

### 応用

- [ ] 音声信号解析
- [ ] 音声への周波数フィルタ


<!--

### 連成振動



物理モデル

固有値・固有ベクトルについて

固有値のベクトルに対応するテンプレートによる吸収。
(固有値の分布毎に意味が対応して存在するという考えのもと、
誤差関数を指標として、複数の周波数特性と意味とが紐付けられるように、吸収する。)

ドレミファ解析
-->


## Credits.

- [素材群](https://www.foobar-onsei-sozai-hogehoge.html/)


## References.

### for Fourier Analysis and its implementing,

- [電子工作のためのフーリエ変換・FFT入門](http://www.maroon.dti.ne.jp/koten-kairo/works/fft/fft_start.html)
- [高速フーリエ変換（FFT） | technical-note](https://hkawabata.github.io/technical-note/note/Algorithm/fft.html)
- [FourierŁÏ−·.dvi](http://www.wannyan.net/scidog/mathbits/fourier.pdf)
- [【フーリエ解析05】高速フーリエ変換(FFT)とは？内側のアルゴリズムを解説！【解説動画付き】](https://kenyu-life.com/2019/07/08/what_is_fft/)


