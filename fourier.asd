(in-package :cl-user)
(defpackage :fourier-asd
  (:use :cl :asdf))
(in-package :fourier-asd)


(asdf:defsystem fourier
  :license nil
  :version "0.0.1"
  :description "Physical Worlds"
  :author "i-makinori"
  :depends-on (cffi clgplot sb-sprof cl-wav zpng)
  :components
  ((:module  "src"
    :components
             ((:file "package")
              (:file "utils")
              (:file "algebra")
              (:file "plotting")
              (:file "fourier")
              ;;
              (:file "sound-signal")
              (:file "experiment")
              ))))



