

;;;; memo
;; imagine of n log n
;; from f(n) = n^2
;; into f(n) =
;;  | n >  c => 2 * f(n/2)
;;  | n <= c => c^2
;;
;; O(n^2) to O(n*log(2,n))

(defun imagine-of-num-fft-calcs (n c)
  (cond ((< c n)
         (* 2
            (imagine-of-num-fft-calcs (/ n 2) c)
            ))
        ((>= c n)
         (expt c 2))))

(defun imagine-of-num-fft-calcs-plot (&key (const 100))
  (loop for i from 0 to 20
        do (let ((num-modes (expt 10 i)))
             (format t "i:~4,A, n:~4,A, n^2:~8,A, f(n):~,A~%"
                     i
                     (log num-modes 10)
                     (log (expt num-modes 2) 10)
                     (log (imagine-of-num-fft-calcs num-modes const) 10)
                     ))))
;;




;;;; DFT which uses sin and cos

(defun area-of-sigmaed-trapezoids-d (series-of-heights)
  (let* ((num-samples (length series-of-heights))
         (delta-bottom (/ 1 num-samples)) ;; if (delta-bottom (/ width num-samples))), true area.
         )
    (* 1/2 delta-bottom
       (+ (+ (* 2 (reduce #'+ series-of-heights)))
          (- (aref series-of-heights 0))
          (- (aref series-of-heights (1- num-samples)))))))

(defun gibbs-blocker (k n)
  ;; https://www.mirai-kougaku.jp/laboratory/pages/200131_02.php
  ;; theta_k(n)
  ;; フェイェールの和
  ;;(/ (+ k (- n) 1)
  ;;(+ k 1))
  ;; id
  (declare (ignore k n))
  1
  )


;;;;

(defun fourier-coefficient-a_0 (function series-of-range kn)
  (declare (ignore kn))
  (let* ((series-of-fxs
           (map 'vector
                #'(lambda (time) (* (funcall function time)
                                    1));;(cos (* kn time))))
                    series-of-range))
         (area ;; area by trapezoids ;; Area is Sigma of partial trapezoid
           (area-of-sigmaed-trapezoids-d series-of-fxs)))
    (* area 2)) ;; if (* area 2 (/ width)), original term.
  )

(defun fourier-coefficient-a_n (function series-of-range kn)
  (let* ((series-of-fxs
           (map 'vector
                #'(lambda (time) (* (funcall function time)
                                    (cos (* kn time))))
                series-of-range))
         (area ;; area by trapezoids ;; Area is Sigma of partial trapezoid
           (area-of-sigmaed-trapezoids-d series-of-fxs)))
    (* area 2)) ;; if (* area 2 (/ width)), original term.
  )

(defun fourier-coefficient-b_n (function series-of-range kn)
  (let* ((series-of-fxs
           (map 'vector
                #'(lambda (time) (* (funcall function time)
                                    (sin (* kn time))))
                series-of-range))
         (area ;; area by trapezoids ;; Area is Sigma of partial trapezoid
           (area-of-sigmaed-trapezoids-d series-of-fxs)))
    (* area 2)) ;; if (* area 2 (/ width)), original term.
  )


;;;;

(defun DFT-coefficients (wave-by-time time0 time1 num-samples num-freq-efficient-max)
  ;;
  (let* (;; 1,2,3,...,n_max
         (mode-n-s
           (coerce (loop for n from 1 to num-freq-efficient-max collect n)
                   'vector))
         ;;(mode-n-s (series-of-range 1 num-frec-efficient-max 1))
         ;; sin(k x) = sin(2 pi f x) => k = 2 pi f, f = 1/T_period
         (freqency (/ 1 (- time1 time0)))
         (omega (* 1 2 *pi* freqency))
         ;;
         (variable-range
           (series-of-range-interval-samples time0 time1 num-samples))
         ;;
         (a-0 nil)
         (efficient-a-list (copy-seq mode-n-s))
         (efficient-b-list (copy-seq mode-n-s))
           )
         ;;
    (setq a-0 (fourier-coefficient-a_0 wave-by-time variable-range nil))
    (loop for mode-n from 1 to num-freq-efficient-max
          do (setf (aref efficient-a-list (1- mode-n))
                   (fourier-coefficient-a_n wave-by-time variable-range (* omega mode-n)))
             (setf (aref efficient-b-list (1- mode-n))
                   (fourier-coefficient-b_n wave-by-time variable-range (* omega mode-n))))
    ;;
    (values a-0 efficient-a-list efficient-b-list)))

(defun dft (wave-by-time time0 time1 num-samples n-max-frec-efficent)
  (let* ((freqency (/ 1 (- time1 time0)))
         (omega (* 1 2 *pi* freqency))
         (k omega))
    (multiple-value-bind (a-0 a-ns b-ns)
        (dft-coefficients wave-by-time time0 time1 num-samples n-max-frec-efficent)
      (lambda (time)
        (let ((num-fx 0))
          (incf num-fx (* 1/2 a-0))
          (loop for n-mode from 1 to n-max-frec-efficent
                do (incf num-fx
                         ;; theta_k(n)*(a_n*cos(n*x)+b_n*sin(n*x))
                         (* (gibbs-blocker n-max-frec-efficent n-mode)
                            (+ (* (aref a-ns (1- n-mode)) (cos (* k n-mode time)))
                               (* (aref b-ns (1- n-mode)) (sin (* k n-mode time)))))))
          num-fx)))))

