(in-package :fourier)


#|
;; notes
-- remark
n: nth, p: period. n,p ∈ Natural
f(n) returns n th amplitude in Series, discete of f(time)
F(p) returns p th frequency in Series, discete of F(frequency=ω/(2*π))
Quasi_Real means computer's none Real numbers as one its implement.

-- types for simulation
Time      :: Quasi_Real -- Discate
Amplitude :: Quasi_Real -- Discete
Frequency :: Quasi_Real -- Discete

Series :: [Quasi_Real]
Spectrum :: [Comples(Quasi_Real, Quasi_Real)]

dft  :: Series   -> Spectrum --         Discete Fourier Transform
idft :: Spectrum -> Series   -- Inverse Discate Fourier Transform

-- fft  is fast version of dft same typed.
-- ifft is fast version of idft same typed.

-- todo types

ApplicationStructre :: -- better name, something there?
. {time_range :: [(Start, Current, End) ∈ Quasi_Real^3] ,
.  series     :: Series,
.  spectrum   :: Spectrum,
.  --...and minimalist things needed.
. }

tfidft :: ApplicationStructre -> (Time -> Amplitude)
. -- especially time_range and spectrum
. -- get Function which links Continuous Quasi_Real Time into Amplitude
. -- quasi_real_Time Function of IDFT

-- also tfifft there.
|#


;;;; exam wave serieses

(defparameter *exam-series-square-32*
  (map 'vector (square-wave 4 1.37) (series-of-range-interval-samples 0 4 32)))

(defparameter *exam-series-delta-256*
  (map 'vector (delta-wave 2 0.95) (series-of-range-interval-samples 0 4 256)))

(defparameter *exam-series-ecos23-400* ;; 400 is not 2^n exam
  (map 'vector #'(lambda (x) (expt (cos x) 23)) (series-of-range-interval-samples 0 10 400)))

(defparameter *exam-series-square-4096*
  (map 'vector (square-wave 4 1.37) (series-of-range-interval-samples 0 12 4096)))


;;;; DFT (Discrete Fourier Transform)


;;;; dft-euler

(defparameter *i* (complex 0 1))
(defparameter *e* (exp 1))

#|
(defun dft-euler-efficients (value-array)
  (let* ((num-samples (length value-array))
         (c_n-s
           (coerce (loop for i from 0 to (1- num-samples) collect (complex 0 0)) 'vector))
         (x_exp (* -1 *i* 2 *pi* (/ num-samples))))
    (loop ;; k = 0,1,...,N-1
          for k from 0 to (1- num-samples)
          do (progn ;; F_k
               (loop
                 for n from 0 to (1- num-samples)
                 do (incf (aref c_n-s k)
                          (* (aref value-array n)
                             (exp (* x_exp k n)))
                          ))))
    c_n-s))

;; (dft-euler (map 'vector #'cos (series-of-range-interval-samples 0 100 1000)))
|#

(defun dft-euler-for-natural-indexes (value-array)
  ;; todo change type
  (let* ((num-samples (length value-array))
         (x_exp (* 1 *i* 2 *pi* (/ num-samples)))
         ;;
         (0-to-n-1
           (coerce (loop for k from 0 to (+ -1 num-samples) collect k) 'vector))
         ;;
         (c_n-s
           (dft value-array))
         ;;
         (dft-ed-function-for-natural-number
           (lambda (n) ;; is all of Real numbers.
             (* 1/1 (/ num-samples)
                (reduce #'+
                        (map 'vector
                             #'(lambda (k) ;; is only Natural numbers.
                                 ;; for natural n s
                                 (let* ((c_k (aref c_n-s k))
                                        (rot (exp (* x_exp k n)))
                                        (rv (* c_k rot)))
                                   rv))
                             0-to-n-1
                             ))))))
    dft-ed-function-for-natural-number))


(defun dft (series)
  ;; > (dft *exam-series-square-32*) ;; #(#C(x.xxxx..., y.yyyy...)...) ;; returns spectrum array.
  (let* ((num-samples (length series))
         (c_n-s
           (coerce (loop for i from 0 to (1- num-samples) collect (complex 0 0)) 'vector))
         (x_exp (* -1 *i* 2 *pi* (/ num-samples))))
    (loop ;; k = 0,1,...,N-1
          for k from 0 to (1- num-samples)
          do ;; F_k
             (loop
               for n from 0 to (1- num-samples)
               do (incf (aref c_n-s k)
                        (* (aref series n)
                           (exp (* x_exp k n))))))
    c_n-s))

(defun spectrum-into-idft-function (spectrum) ;; idft is not ifft. it is not fast
  (let* ((num-samples (length spectrum))
         ;; (rotation-base (* 1 *i* 2 *pi* (/ num-samples)))
         (0-to-n-1
           (coerce (loop for k from 0 to (+ -1 num-samples) collect k) 'vector)))
    (lambda (n)
      (* 1/1 (/ num-samples)
         (reduce #'+
          (map 'vector
               #'(lambda (k)
                   (let* ((c_k (aref spectrum k))
                          ;;(rot (expt rotation-base (* k n)))
                          (rot (w-rotation num-samples (* -1 k n)))
                          (rv (* c_k rot)))
                     ;;(print c_k)
                     rv))
               0-to-n-1))))))


(defun dft-euler (value-array)
  (let* ((dft-ed-function-for-natural-number
           (dft-euler-for-natural-indexes value-array)
           ;;(fft-poor-euler-for-natural-indexes value-array)
           ))
    ;; approx for Real numbers implement
    (lambda (n)
      (let* ((n+0 (+ 0 (floor n)))
             (n+1 (+ 1 (floor n)))
             (delta (- n n+0))
             (f_n+0 (funcall dft-ed-function-for-natural-number n+0))
             (f_n+1 (funcall dft-ed-function-for-natural-number n+1))
             (approx-f_n (+ f_n+0 (* delta (- f_n+1 f_n+0)))))
        approx-f_n
        ))
    ;; for only Natural numbers implement
    dft-ed-function-for-natural-number
    ))


;;;; FFT (Fast Fourier Transform)

;; bits

(defun reverse-bit-until-digit (integer digit)
  ;; @> (reverse-bit-until-digit 3 4) ;; 12
  ;; @> (reverse-bit-until-digit 210 8) ;; 75
  (let* ((number-to-shave integer)
         (tmp 0))
    (loop for i from 0 to (1- digit)
          do (setq tmp (logior (ash tmp 1) (logand number-to-shave 1)))
             (setq number-to-shave (ash number-to-shave -1)))
    ;;(format t "from ~D (~@b) digit ~D.~%" integer integer digit)
    ;;(format t "into ~D (~@b) .~%note, fill (+___...) with 0s depend on digit~%" tmp tmp)
    tmp))


(defun reversed-bit-indexeses-for-fft (num-samples)
  ;; @> (reversed-bit-indexeses-for-fft 4) ;; #(0 2 1 3)
  ;; @> (reversed-bit-indexeses-for-fft 5) ;; #(0 4 2 6 1 5 3 7) ;; |2^2| < |5| ≤ |2^3|
  (let* ((exponent (ceiling (log num-samples 2)))
         (length (expt 2 exponent))
         (indexes
           (map 'vector #'(lambda (num) (reverse-bit-until-digit num exponent))
                (coerce (loop for i from 0 to (1- length) collect i) 'vector))))
    indexes))


;; labels

#|
(defun aref-but-0-if-empty (series index)
  (handler-case (aref series index)
(sb-int:invalid-array-index-error (e) (declare (ignore e)) 0)))

(defun bit-reversed-series-for-fft (series)
  "bit-reversed indexed complex inputting series, filled with 0 while 2^n length."
  (map 'vector
       #'(lambda (index_reversed)
           (complex (aref-but-0-if-empty series index_reversed) 0))
       (reversed-bit-indexeses-for-fft (length series))))
|#

(defun index-exist-p (series index)
  (and (integerp index)
       (<= 0 index)
       (< index (length series))))

(defun aref-and-complex-it-but-0-0-if-it-is-empty (series index)
  (cond ((not (index-exist-p series index))
         (complex 0 0))
        ((complexp (aref series index))
         (aref series index))
        ((numberp (aref series index))
         (complex (aref series index) 0))
        (t (complex 0 0))))

(defun bit-reversed-series-for-fft (series)
  "bit-reversed indexed complex inputting series, filled with 0 while 2^n length."
  (map 'vector
       #'(lambda (index_reversed)
           (aref-and-complex-it-but-0-0-if-it-is-empty series index_reversed))
       (reversed-bit-indexeses-for-fft (length series))))



(defun rotation-powers-of-step (step exponent)
  ;; step are counted from {1, 2, ..., to exponent}.
  ;; exponent is exponent of num-samples.
  ;; @> (rotation-powers-of-step 1 3) ;; #(0 0 0 0 0 0 0 0)
  ;; @> (rotation-powers-of-step 2 3) ;; #(0 0 0 1 0 0 0 1)
  ;; @> (rotation-powers-of-step 3 3) ;; #(0 0 0 0 0 1 2 3)
  ;; note: make (W_N)^x rotation operator matrix, look to bits, there is laws of rotation powers.
  (let ((rots ;; a lot's of rotations. don't be rot please.
          (make-array (list (expt 2 exponent)) :initial-element 0)))
    (loop for i from 0 to (1- (expt 2 exponent))
          do (setf (aref rots i)
                   (if (logbitp (- step 1) i)
                       ;; (expt 2 n) is maybely slow, (2^n)-1 is #...0000111111...
                       (logand (- (expt 2 (- step 1)) 1) i)
                       0)))
    ;; (format t "~%~A: ~A~%" step rots)
    rots))


#|
(defun shove-insert-bit (n-bit 0-or-1 num)
  (let ((fold-n num)
        (tmp 0))
    (loop for i from 0 to 100
          until (zerop fold-n)
          do (cond ((= n-bit i)
                    (setf tmp (logior (ash tmp 1) 0-or-1)))
                   (t
                    (setf tmp (logior (ash tmp 1)
                               )))
          ))
        tmp
    ))

(defun s-steped-quasi-even-numbers (s len)
  ;; semi 2n(s) numbers of s-th step
  (loop for i from 0 to (1- len)
        collect i
  ))

(defun s-steped-quasi-odd-numbers (s len)
  (loop for i from 0 to (1- len)
        collect i
        ))
|#


(defun quasi-even-number-p (num step)
  (not (logbitp (- step 1) num))
  ;; ...0... -> num is quasi-even at step -> t
  ;; ...1... -> num is quasi-odd  at step -> nil
  )


(defun series-whiches-t-indexes-are-quasi-evens-at-step (0-to-n step)
  ;; @> (series-t-indexs-is-steps-quasi-evens 7 1) ;; #(T NIL T NIL T NIL T NIL)
  ;; @> (series-t-indexs-is-steps-quasi-evens 7 2) ;; #(T T NIL NIL T T NIL NIL)
  ;; @> (series-t-indexs-is-steps-quasi-evens 7 3) ;; #(T T T T NIL NIL NIL NIL)
  ;; @> (series-t-indexs-is-steps-quasi-evens 7 4) ;; #(T T T T T T T T)
  (coerce (loop for num from 0 to 0-to-n
                collect (quasi-even-number-p num step))
          'vector))

(defun butterfly-for-fft (series-in step)
  "step must to be 1, 2, ..., (ceiling (log (length series-in) 2))"
  ;; @> (butterfly #(0 1 2 3) 0) ;; Error. ;; 1/2 index
  ;; @> (butterfly #(0 1 2 3) 1) ;; #(1 -1 5 -1)
  ;; @> (butterfly #(0 1 2 3) 2) ;; #(2 4 -2 -2)
  ;; @> (butterfly #(0 1 2 3) 3) ;; Error.  ;; out of range
  ;; @> (butterfly #(0 1 2 3 4) 2) ;; mostly Error. length of series-is is not 2^n.
  (let* ((series-out (make-array (length series-in) :initial-element 0))
         (delta_i-for-peri-demi (expt 2 (- step 1))))
    (loop for i from 0 to (1- (length series-in))
          do (setf (aref series-out i)
                   (if (quasi-even-number-p i step)
                       ;; if quasi even at step i
                       (+ (+ (aref series-in i))
                          (+ (aref series-in (+ i delta_i-for-peri-demi))))
                       ;; if quasi odd  at step i
                       (+ (+ (aref series-in (- i delta_i-for-peri-demi)))
                          (- (aref series-in i))))))
    series-out))


;; FFT

(defun w-rotation (&optional (n-div 1) (nk-power 1))
  (expt *e* (* -2 *pi* *i* (/ n-div) nk-power)))


(defun fft (series)
  (let* ((num-samples (length series))
         (exponent (ceiling (log num-samples 2)))
         (folding-series ;; "gasagoso"
           (bit-reversed-series-for-fft series))
         )
    ;; "pata-shae, pata-shae ..."
    (loop for step from 1 to exponent ;;from 0 to (1- exponent)
          do ;;(format t "~%step: ~A~%" step) ;; if formatp is t
             (setf folding-series
                   (butterfly-for-fft
                    (map 'vector
                         #'(lambda (rotation-power val)
                             (* val
                                (w-rotation (expt 2 step)
                                            rotation-power)))
                         (rotation-powers-of-step step exponent)
                         folding-series)
                    step)))
    
    ;; "don"
    folding-series
    ;; length |2^a| to |N|
    ;;(subseq folding-series 0 num-samples)
    ;;
    ;;(subseq folding-series 0 (ash (length folding-series) -1))
    ))

;;;; IFFT

(defun ifft (spectrum)
  (let ((frequency-width (length spectrum)))
    (map 'vector #'(lambda (sgm) (/ sgm frequency-width))
         (fft spectrum))))


;;;; spectrum explicar functions
;; ref: https://ja.wikipedia.org/wiki/%E3%82%B9%E3%83%9A%E3%82%AF%E3%83%88%E3%83%AB%E5%AF%86%E5%BA%A6


(defun power-spectre-gen ()
  (lambda (complex)
    (* (conjugate complex) complex)))

(defun energy-spectre-gen (&optional (delta-time-for-sampling 1.00) (length *pi*))
  (lambda (complex) ;; and needed args
    (* (expt delta-time-for-sampling  2)
       (/ (sqrt (* 2 length)))
       (conjugate complex) complex)))



(defun something-spectre ()
  ())




;;;; plot spectrum


(defun plot-series-es (series-list)
  ;; @> (plot-series-es (list (ifft (fft *exam-series-square-32*)) *exam-series-square-4096*))
  (let* ((plotting-list
           (apply #'append (mapcar #'(lambda (series)
                                      (list (map 'vector #'realpart series)
                                            (map 'vector #'imagpart series)))
                                  series-list)))
         (x-seq-list
           (mapcar
            #'(lambda (value-array)
                (series-of-range-interval-samples
                 0 (- (length value-array) 1) (length value-array)))
            plotting-list)))
    (plots plotting-list
           :x-seqs x-seq-list
           :output *pathname-of-examed-graph*
           :output-format :pdf)
    (values series-list *pathname-of-examed-graph*)))

(defun plot-series (series)
  ;; @> (plot-series *exam-series-ecos23-400*)
  ;; @> (plot-series (ifft (dft *exam-series-ecos23-400*)))
  ;; @> (plot-series (fft *exam-series-ecos23-400*))
  ;; @> (plot-series (ifft (fft *exam-series-ecos23-400*)))
  (plots (list (map 'vector #'realpart series)
               (map 'vector #'imagpart series))
         :output *pathname-of-examed-graph*
         :output-format :pdf)
  (values series *pathname-of-examed-graph*))

(defun plot-spectrum (explican spectrum)
  ;; @> (plot-spectrum (power-spectre-gen) (fft *exam-series-delta-256*))
  ;; @> (plot-spectrum (power-spectre-gen) (dft *exam-series-delta-256*))
  (let* ()
    (plots (list (map 'vector #'(lambda (freq) (realpart (funcall explican freq))) spectrum))
           :output *pathname-of-examed-graph*
           ;:x-seqs (list x-seq)
           :output-format :pdf)
    (values spectrum *pathname-of-examed-graph*)))


;;;; consecutive times
#|
(defun ifft-function-for-floating-time (spectrum its-time-from its-time-ended-at)
)


(defun check-transform-for-consecutive-real-codomain (consective-real-function
                                                      transforming-function
                                                      inverse-transforming-function
                                                      ;;
                                                      defined-codomain-min
                                                      defined-codomain-max
                                                      num-samples-for-defined-codomain
                                                      ;;
                                                      exam-codomain-min
                                                      exam-codomain-max
                                                      num-samples-for-exam-codomain)
  (let* ((defined-codomain
          (series-of-range-interval-samples
           defined-codomain-min defined-codomain-max num-samples-for-defined-codomain))
         (exam-codomain
           (series-of-range-interval-samples
            exam-codomain-min exam-codomain-max num-samples-for-exam-codomain)))
        )))
  |#


#|

;;; for quasi_real time
(defun plots-idft-dft-from-series (series)
  ;; (plots-idft-dft-from-series *exam-series-square-32*)
  ;; (plots-idft-dft-from-series *exam-series-ecos23-400*)
  (let* ((dft-ed-function
           ;;(dft-euler-for-natural-indexes series))
           (dft-euler series))
         (n-list
           (coerce (loop for i from 0 to (1- (length series)) collect i) 'vector))
         (n-exam-additional 20)
         (real-or-natural-number-test
           1   ;; for Naturals
           ;;0.1 ;; for Reals
           )
         (test-list
           (coerce (loop for i from (- n-exam-additional) to (+ n-exam-additional -1 (length series))
                         collect (* i real-or-natural-number-test))
                   'vector))
           )
    (plots (list (map 'vector #'(lambda (n) (aref series n)) n-list)
                 (map 'vector #'(lambda (n) (realpart (funcall dft-ed-function n)))
                      test-list)
                 (map 'vector #'(lambda (n) (imagpart (funcall dft-ed-function n)))
                      test-list)

                 )
           :x-seqs (list n-list test-list test-list)
           :output *pathname-of-examed-graph*
           :output-format :pdf)
    dft-ed-function))
|#



#|
(defun plots-dft-calcs (function time0 time1 num-samples n-max-frec-efficent)
  (let* ((dft-ed-function
           (dft function time0 time1 num-samples n-max-frec-efficent))) ;; old
    (plot-functions
     (list function dft-ed-function)
     (- time0 8)
     (+ time1 8)
     0.001
     :title-list (list "true function"
                       (format nil "fouriered function, N = ~A" n-max-frec-efficent)))
    
    dft-ed-function))

;; (plots-dft-calcs (delta-wave 10 2) 0 100 1000 10)
;; (plots-dft-calcs (square-wave 4 1) -8 8 1000 100)
;; (plots-dft-calcs (delta-wave 10 2) 0 100 1000 10)
;; (plots-dft-calcs #'(lambda (x) (expt (cos x) 23)) 0 10 1000 100)
|#

;;;; profile
;;
;; (time (dft quare-wave 2 1) 0 10 10000 500))


#|
;; dft, function name is old.
(defun profiling-fourier-process ()
    (let* ((original-function (square-wave 2 1))
           (n-samples 10000)
           (n-max 500)
           (fouriered-function #'identity))
      (time
       (sb-sprof:with-profiling (:max-samples 10
                                 ;;:report :graph ;; more detaily
                                 :report :flat
                                 :loop nil)
         (setf fouriered-function (dft original-function 0 10 n-samples n-max)))) ;; old
      (plot-functions (list original-function fouriered-function)
                      -5 15 0.001
                      :title-list (list (format nil "true-function")
                                        (format nil "fourierd function, N=~a" n-max)))
      fouriered-function))

|#
