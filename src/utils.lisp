(in-package :fourier)


;;

(defparameter
    *pathname-of-this-system*
  (asdf:system-relative-pathname :fourier ""))

(defparameter
    *pathname-of-generated-directory*
  (merge-pathnames "tmp/" *pathname-of-this-system*))

(defparameter
    *pathname-of-examed-graph*
  (merge-pathnames "test.pdf" *pathname-of-generated-directory*))

