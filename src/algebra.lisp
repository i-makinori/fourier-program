(in-package :fourier)


;;;;


(defparameter *pi* 3.141592653589793d0)


;;;; wave

#|

;; delta wave (example of wave).
___f(time)
____ |
+1.0 |____*_______________
+0.5 |__*****_____________
_0.0 |_*******_*********_------ 0
-0.5 |____________*****___
-1.0 |______________*_____
____ ^----^----^----^----^------ time
____ |   0.25 0.50 0.75 1.00
|#


(defun delta-wave (time-period max)
  ;; generate delta wave
  (lambda (time)
    (let ((mod-time (mod time time-period)))
      (cond
        ((< mod-time (* 1/4 time-period))
         (+ (* +4 (/ time-period) max mod-time)
            0))
        ((< mod-time (* 3/4 time-period))
         (+ (* -4 (/ time-period) max mod-time)
            (* +2 max)))
        ((< mod-time (* 4/4 time-period))
         (+ (* 4 (/ time-period) max mod-time)
            (* -4 max)))
        ;; (t 0)
        ))))

(defun square-wave (time-period max)
  (lambda (time)
    (let ((mod-time (mod time time-period)))
      (cond
        ((< mod-time (* 1/2 time-period))
         (+ max))
        ((and t (< mod-time (* 1/1 time-period)))
         (- max))
        (t 0)))))

;;(defparameter *delta-1-waves*
;;  (values-of-function (delta-wave 8 2) 0 12 0.1))



;;;; range and wave's values

(defun series-of-range (val-from val-to abs-delta)
  ;; There is Bugs at edge and boundings.
  ;; [val-from <= x (< or <=) val-to] 
  (let* ((delta (* (abs abs-delta)
                   (if (plusp (- val-from val-to)) -1 1)))
         (len (+ 1 (abs (floor (* (/ 1 abs-delta) (- val-to val-from))))))
         (arr (make-array (list (abs len)) )))
    (loop for i from 0 to (1- len)
          do (setf (aref arr i)
                   (+ val-from (* delta i))))
    arr))

(defun series-of-range-1 (v-min v-max delta)
  (loop for v-i from v-min to v-max by delta collect v-i)
  )

(defun series-of-range-interval-samples (val-min val-max n-samples)
  ;; interval-range
  ;; [a,b], ●----● ,閉区間
  ;; topic of n or n + 1
  ;; v_min <= x <= v_max
  ;; [x] is [v_min,v_min+delta,...,≒v_max]
  ;; length of [x] is 1 + (sample - 2) + 1
  (let ((delta (/ (- val-max val-min) (1- n-samples)))
        (arr (make-array n-samples))
        )
    (loop for i from 0 to (1- n-samples)
          do (setf (aref arr i)
                   (+ val-min (* i delta))))
    arr))


(defun values-of-function (function val-from val-to delta)
  (map 'vector
       #'(lambda (x)
           (let ((fx (funcall function x)))
             ;;(format t "~4f: ~A~%" x fx)
             fx))
       (series-of-range val-from val-to delta)))

