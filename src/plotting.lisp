(in-package :fourier)


;;;; plottings

#|
;; plotting example.
;; from (https://masatoi.hateblo.jp/entry/20160323/1458719398)
;;
(defparameter *x-list* (loop for i from (- pi) to pi by 0.1 collect i))
(plots (list (mapcar #'sin *x-list*)
             (mapcar #'cos *x-list*)
             (mapcar #'tan *x-list*))
       :x-seqs (list *x-list* *x-list* *x-list*)
       :x-range (list (- pi) pi)
       :y-range '(-1 1)
       :title-list '("sin" "cos" "tan")
       :x-label "x"
       :y-label "f(x)"
       :output "./picture.png")
|#



(defun plot-function (function val-from val-to delta)
  ;; todo series of range-rect to series-of-range-interval-samples
  (let* ((xs (series-of-range val-from val-to delta))
         (ys (map 'vector function xs)))
    (plot ys
          :x-seq xs
          :y-label "f(x)"
          :x-label "x"
          :output *pathname-of-examed-graph*
          :output-format :pdf)))

#|
;; example
(plot-function (delta-wave 8 2) 0 12 0.01)
|#

(defun plot-functions (function-list val-from val-to delta &key (title-list nil))
  ;; todo series of range-rect to series-of-range-interval-samples
  (let* ((xs (series-of-range val-from val-to delta))
         (xss
           (mapcar #'(lambda (f) f xs) function-list))
         (yss (mapcar
               #'(lambda (function xs)
                   (map 'vector function xs))
               function-list
               xss)))
        (plots yss
          :x-seqs xss
          :y-label "f(x)"
          :x-label "x"
          :output *pathname-of-examed-graph*
          :output-format :pdf
          :title-list title-list
          ;;:x-range (list -4 4)
          ;;:y-range (list -2 2)
          :key t)))

