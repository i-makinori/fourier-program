(in-package #:fourier)

;;;; util

#|
(defun keyth (key nest-list)
  (cond ((null nest-list) nil)
        ((eq key (car nest-list)) (cadr nest-list))
        ((listp (car nest-list)) (cons (keyth key (car nest-list))
                                       (keyth key (cdr nest-list))))
        (t (keyth key (cddr nest-list)))))
|#

;;;; parameters

(defparameter *pathname-of-data-directory*
  (merge-pathnames "data/" *pathname-of-this-system*))

(defparameter *wav-file-name-list*
  (directory (merge-pathnames "*.wav" *pathname-of-data-directory*)))


(defun re-register-wav-file-name-list! ()
  (defparameter *wav-file-name-list*
    (directory (merge-pathnames "*.wav" *pathname-of-data-directory*)))
  *wav-file-name-list*)

(progn
  (defparameter *wav-file-name-list* nil)
  (re-register-wav-file-name-list!)
  *wav-file-name-list*)


(defparameter *wav-file-example1*
  (nth 0 *wav-file-name-list*))


;;;; wave-data

(defstruct (wave-data)
  (:pathname #P"")
  (:num-samples 0)
  (:sample-per-second 1)
  ;;(:bit-per-sample 8)
  (:num-channels 2)
  (:playing-time 0)
  (:wave-series #())
  ;;
  (:fourier-analysis '())
  )


;;;; cl-wav

#|
(wav:read-wav-file
 (nth 0 *wav-file-name-list*)
 :chunk-data-reader (wav:wrap-data-chunk-data-samples-reader))
|#

(defun keyth (key-name list)
  (cond ((null list) nil)
        ((eq key-name (car list)) (cadr list))
        (t (keyth key-name (cddr list)))))

(defun chunkth (chunk-id list)
  "todo search chunk by chunk-id"
  (cond ((equal (cadr (car list)) chunk-id)
         (car list))
        ((null list) nil)
        (t
         (chunkth chunk-id (cdr list)))))

#|
'((:CHUNK-ID "RIFF" :CHUNK-DATA-SIZE 3848860 :FILE-TYPE "WAVE")
  (:CHUNK-ID "fmt " :CHUNK-DATA-SIZE 16 :CHUNK-DATA
   (:COMPRESSION-CODE 1 :NUMBER-OF-CHANNELS 2 :SAMPLE-RATE 44100
    :AVERAGE-BYTES-PER-SECOND 176400 :BLOCK-ALIGN 4 :SIGNIFICANT-BITS-PER-SAMPLE
    16))
  (:CHUNK-ID "data" :CHUNK-DATA-SIZE 3848824 :CHUNK-DATA
   "series #(-0.03237915 0.007873535 -0.011199951 -6.4086914e-4 -0.0028076172
         -0.015167236 -0.009094238 -0.03378296 -0.016540527 -0.04840088
         -0.02154541 -0.05545044 -0.014282227 -0.046691895 7.9345703e-4
         -0.031982422 0.0077209473 -0.033813477 0.007904053 -0.048583984
         0.015777588 -0.06503296 0.036956787 -0.07394409 0.047973633
         -0.08605957 0.042938232 -0.097351074 0.033355713 -0.10202026
         0.030548096 -0.09603882 0.023468018 -0.09277344 0.010772705
         -0.092926025 0.0038146973 -0.08920288 0.009552002 -0.07904053
         0.024291992 -0.06222534) and long more."))
|#

#|
'((:CHUNK-ID "RIFF" :CHUNK-DATA-SIZE 1411488 :FILE-TYPE "WAVE")
  (:CHUNK-ID "fmt " :CHUNK-DATA-SIZE 16 :CHUNK-DATA
   (:COMPRESSION-CODE 1 :NUMBER-OF-CHANNELS 2 :SAMPLE-RATE 44100
    :AVERAGE-BYTES-PER-SECOND 176400 :BLOCK-ALIGN 4 :SIGNIFICANT-BITS-PER-SAMPLE
    16))
  (:CHUNK-ID "LIST" :CHUNK-DATA-SIZE 96 :FILE-TYPE "INFO")
  (:CHUNK-ID "INAM" :CHUNK-DATA-SIZE 28 :CHUNK-DATA
   #(227 131 137 227 131 172 227 131 159 227 131 149 227 130 161 227 130 189 227
     131 169 227 130 183 227 131 137 0))
  (:CHUNK-ID "IPRD" :CHUNK-DATA-SIZE 25 :CHUNK-DATA
   #(227 131 157 227 130 177 227 131 131 227 131 136 227 130 181 227 130 166 227
     131 179 227 131 137 0))
  (:CHUNK-ID "ISFT" :CHUNK-DATA-SIZE 14 :CHUNK-DATA
   #(76 97 118 102 53 57 46 50 55 46 49 48 48 0))
  (:CHUNK-ID "data" :CHUNK-DATA-SIZE 1411348 :CHUNK-DATA
   "series #(3.3569336e-4 0.0 5.187988e-4 3.0517578e-5 6.1035156e-4 3.0517578e-5
         7.019043e-4 -1.5258789e-4 6.713867e-4 -4.272461e-4 6.4086914e-4
         -5.493164e-4 6.4086914e-4 -4.272461e-4 7.019043e-4 -4.5776367e-4
         5.187988e-4 -6.713867e-4 1.5258789e-4 -8.239746e-4 -9.1552734e-5
         -7.9345703e-4 -1.2207031e-4 -7.019043e-4 -2.4414063e-4 -6.4086914e-4
         -4.272461e-4 -6.713867e-4 -5.79834e-4 -7.019043e-4 -8.239746e-4
         -5.79834e-4 -8.8500977e-4 -5.187988e-4 -7.9345703e-4 -4.5776367e-4
         -9.1552734e-4 -3.6621094e-4 -0.0010070801 -3.6621094e-4 -9.765625e-4
         -2.746582e-4) and long more."))
|#


(defun read-wav-file-into-wave-data (wav-file)
  (let ((sound (wav:read-wav-file
                 wav-file
                 :chunk-data-reader (wav:wrap-data-chunk-data-samples-reader))))
    (if sound
        (let* ((fmt-data (keyth :chunk-data (chunkth "fmt " sound)))
               (sample-per-second (keyth :sample-rate fmt-data))
               (num-channels
                 (keyth :number-of-channels fmt-data))
               (wave-series (keyth :chunk-data (chunkth "data" sound)))
               (num-samples (length wave-series)))
          (make-wave-data
           :pathname wav-file
           :num-samples num-samples
           :num-channels num-channels
           :sample-per-second sample-per-second
           :playing-time (* num-samples (/ sample-per-second) (/ 1 num-channels))
           :wave-series wave-series
           :fourier-analysis nil)))))



(defun format-wave-data-info (wave-data)
  ;; @> (format-wave-data-info (read-wav-file-into-wave-data *wav-file-example1*))
  #|
  (concatenate
   'string
   (map
    #'(lambda (key-name)
        (format nil "~A ~A~%"
                key-name
                (if (vectorp (slot-value wave-data key-name))
                    (let ((sub-vector
                            (subseq (slot-value keys-of-wave-data key-name)
                                    0 (min 20 (length (slot-value keys-of-wave-data key-name))))))
                      (format nil "series ~A and more" sub-vector))
                    (slot-value wave-data key-name))))
  (key-list-of-struct wave-data)
  |#
  (format t ":pathname ~A~%" (wave-data-pathname wave-data))
  (format t ":num-samples ~A~%" (wave-data-num-samples wave-data))
  (format t ":sample-per-second ~A~%" (wave-data-sample-per-second wave-data))
  (format t ":bum-samples ~A~%" (wave-data-num-channels wave-data))
  (format t ":playing-time ~A~%" (float (wave-data-playing-time wave-data)))
  (format t ":wave-series ~A ... and more~%"
          (subseq (wave-data-wave-series wave-data)
                  0 (min 31 (max 0 (1- (length (wave-data-wave-series wave-data)))))))
  (format t ":fourier length is ~A~%" (length (wave-data-fourier-analysis wave-data)))
  (format t "length wave-series is ~A~%" (length (wave-data-wave-series wave-data)))
    )

(defun write-wave-from-wave-wtruct (wave-struct &optional write-filename)
;; ref: https://github.com/RobBlackwell/cl-wav/blob/master/wav.lisp
;; (defun write-wav-file (chunks filespec &key (chunk-data-writer (wrap-format-chunk-data-writer)))
  ;;  (riff:write-riff-file chunks filespec :chunk-data-writer chunk-data-writer))
  wave-struct write-filename
  nil
  )

(defun keys-of-wave-data (&optional (wav-file *wav-file-example1*))
  (mapcar #'(lambda (l)
              (mapcar #'(lambda (n)
                          (if (and (vectorp n) (< 42 (length n)))
                              (format nil "series ~A and long more." (subseq n 0 42))
                              n))
                      l))
          (wav:read-wav-file
           wav-file
           :chunk-data-reader (wav:wrap-data-chunk-data-samples-reader))))

;;;; plot

(defun plot-wave-data (wave-data num-points-to-plot)
  (let* ((dedomain (map 'vector #'floor
                        (series-of-range-interval-samples 0 (1- (wave-data-num-samples wave-data))
                                                          num-points-to-plot)))
         (codomain (map 'vector #'(lambda (n) (aref (wave-data-wave-series wave-data) n))
                        dedomain)))
    (plot-series codomain)))


;;;; convolution

(defun convolution-model (length window-size slide-size)
  (let* ((num-windows (ceiling (/ (- length window-size) slide-size)))
         (window-list nil))
    window-list
    (loop for current-st-pos from 0 to num-windows ;; not current-st-pos its i
          collect (list (* current-st-pos slide-size)
                        (min (+ (* current-st-pos slide-size) window-size)
                             (1- length))))))


(defun copy-series-in-size (series size &optional (data-if-fill 0))
  (let ((array-new (make-array size :initial-element data-if-fill)))
    (loop for i from 0 to (1- (min (length series) (length array-new)))
          do (setf (aref array-new i)
                   (aref series i)))
    array-new))

(defun convolution-fft-series (series window-size slide-size &optional (n-format-status 0))
  ;; @> (length (convolution-fft-series *exam-series-square-4096* 120 39))
  ;; window-size << series ;; enough less and less than .
  (let* ((num-windows (ceiling (/ (- (length series) window-size) slide-size)))
         (size-by-frequency (expt 2 (ceiling (log window-size 2))))
         (spectrogram-array
           (make-array (list num-windows))))
    (loop for iter from 0 to (1- num-windows)
          do
          (let* ((st (* iter slide-size))
                 (ed (min (+ st window-size) (- (length series) 1)))
                 (series-chunked
                   (copy-series-in-size
                    (subseq series st ed)
                    size-by-frequency ;; todo 2^n size
                    0)))
            (setf (aref spectrogram-array iter)
                  (fft series-chunked)))
          (if (and n-format-status
                   (or (zerop iter)
                       (= 0 (mod iter (floor (/ num-windows n-format-status))))))
              (format t "iter/num-windows : ~A / ~A~%" iter num-windows))
          )
    spectrogram-array))


(defun spectrogram-of-series (series window-size slide-size)
  ;; @> (length (aref (spectrogram-of-series *exam-series-square-4096* 200 10) 0))
  (convolution-fft-series series window-size slide-size 24)
  )

;;;; plot spectorgram


(defparameter *spectrogram-filename*
  (merge-pathnames "spectrogram.png" *pathname-of-data-directory*))

(defun value-into-byte-value (value)
  (floor (max 0 (min 255 value))))


(defparameter *vision-emphasis-adjustment* 128)

(defun sound-level-db (p-noise p-zero)
  (handler-case
      ;;(* 20 (log (/ p-noise p-zero) 10))
      (* *vision-emphasis-adjustment* (log (/ p-noise p-zero) 10))
    (division-by-zero (e) e 0)))

(defun line-value-into-circle-theta (n-r1+i0-as-n)
  ;; real line's value into point at infinity
  n-r1+i0-as-n
  0)

(defun circle-theta-into-line-value (theta-of-crossing)
  ;; point at infinity into real line's value
  theta-of-crossing
  0)


(defun complex-value-into-color-bit (complex)
  ;; todo, dont kill high and low.
  ;; limitting 0-255 does not show plot spectrogram.
  ;; remap {-infinity to +infinity} range into {-1 to +1}.
  (let (;; power spectre bit
        ;;(r (+ 32))
        ;;(g (+ 0 (floor (max 0 (min 255 (expt (imagpart complex) 2))))))
        ;;(b (+ 0 (floor (max 0 (min 255 (expt (realpart complex) 2))))))
        ;;(a 255)
        ;;
        ;; sound-level bit
        (r 32)
        (g (+ 0 (value-into-byte-value (sound-level-db (expt (imagpart complex) 2) 1))))
        (b (+ 0 (value-into-byte-value (sound-level-db (expt (realpart complex) 2) 1))))
        (a 255)
    )
    (list r g b a )))

;; @> (draw-spectrogram (spectrogram-of-series *exam-series-square-4096* 256 4) *spectrogram-filename*)
;; @> (draw-spectrogram (spectrogram-of-series *exam-series-ecos23-400* 256 1) *spectrogram-filename*)

;; FUN!!
;; @> (draw-spectrogram (spectrogram-of-series (wave-data-wave-series (read-wav-file-into-wave-data *wav-file-example1*)) 256 128) *spectrogram-filename*)

(defun draw-spectrogram (spectrogram file)
  (let* (;; array's array is not 2d matrix. but, length of all freq dim is same.
         (time-dim-discreted (length spectrogram))
         (freq-dim-discreted (length (aref spectrogram 0)))
         ;; png
         (png (make-instance 'zpng:pixel-streamed-png
                             :color-type :truecolor-alpha ;; :grayscale-alpha :grayscale will fast
                             :width time-dim-discreted :height freq-dim-discreted)))
    (with-open-file (stream file
                            :direction :output
                            :if-exists :supersede
                            :if-does-not-exist :create
                            :element-type '(unsigned-byte 8))
      (zpng:start-png png stream)
      (loop for discre-frequency from 0 to (- freq-dim-discreted 1)
            do (if (zerop (mod discre-frequency 100))
                   (format t "loop: ~A~%" discre-frequency ))
               (loop for discre-time from 0 to (- time-dim-discreted 1)
                     do (zpng:write-pixel

                         (complex-value-into-color-bit
                          (aref (aref spectrogram discre-time) discre-frequency))
                         png)))
      (format t "~%")
      (format t "width: ~A, height: ~A~%" time-dim-discreted freq-dim-discreted)
      (format t "filename: ~A~%" file)
      (zpng:finish-png png))))

;; matrix tools

(defun make-array-array (w-length h-length &optional (initial 0.0))
  (make-array w-length :initial-element
              (make-array h-length :initial-element initial)))

(defun 2d-matrix-into-array-array (2d-matrix)
  (let* ((len-row (array-dimension 2d-matrix 0))
         (len-col (array-dimension 2d-matrix 1))
         (array-array1 (make-array (list len-row))))
    (loop for row from 0 to (1- len-row)
          do (let* ((col-vec! (make-array (list len-col))))
               (loop for col from 0 to (1- len-col)
                     do (setf (aref col-vec! col) (aref 2d-matrix row col)))
               (setf (aref array-array1 row) col-vec!)))
    array-array1))

(defun array-array-into-2d-matrix (array-array &optional (fill-with 0))
  (let* ((d1-len (length array-array))
         (d2-len (apply #'max (coerce (map 'vector #'length array-array) 'list)))
         (matrix (make-array (list d1-len d2-len) :initial-element fill-with)))
    (dotimes (d1 d1-len 0)
      (dotimes (d2 d2-len 0)
        ;;(format t "~A ~A, ~A ~%" d1 d2 matrix)
        (setf (aref matrix d1 d2)
              (handler-case 
                  (aref (aref array-array d1) d2)
                (sb-int:invalid-array-index-error (e) e fill-with)))))
    matrix))

#|
(defun draw-spectrogram (spectrogram file)
  ;; with gnuplot
  (let* ((matrix (array-array-into-2d-matrix spectrogram)))
    (print (array-dimensions matrix))
    (splot-matrix matrix
                  ;:x-label "time[nth window]"
                  ;:y-label "frequency[n times multiply]"
                  ;:z-label "frequency strength of its time [db(?)]"
                  ;:output file
                  )))
|#

(defun draw-wave-file-spectrogram (wav-file png-file window-size slide-size)
  ;; @> (draw-wave-file-spectrogram *wav-file-example1* *spectrogram-filename* 256 128)
  (let ((wave-data nil)
        (spectrogram nil)
        (png-llwrittingled nil))
    ;;
    (format t "read wave wile into wave-data: ~A.~%" wav-file)
    (setf wave-data (read-wav-file-into-wave-data wav-file))
    ;;
    (format t "fold wave-data into spectrogram: ~A.~%" (format-wave-data-info wave-data))
    (setf spectrogram
          (spectrogram-of-series (wave-data-wave-series wave-data) window-size slide-size))
    ;;
    (format t "write spectrogram into png image: ~A.~%" png-file)
    (setf png-llwrittingled (draw-spectrogram spectrogram png-file))
    png-llwrittingled
    nil))


;;; diff-spectrogram

;; forward difference
;; df(t)/dt ~= ( {f(t+1) - f(t)} +  / {1 * dt}) + O(dt)

(defun diff-of-horizontal (2d-matrix n_x n_y &optional (d_time 1))
  (/ (abs (- (aref 2d-matrix (+ n_x 1) n_y)
             (aref 2d-matrix (+ n_x 0) n_y)))
     (* d_time)))

(defun diff-of-vertical (2d-matrix n_x n_y &optional (d_time 1))
  (/ (abs (- (aref 2d-matrix n_x (+ n_y 1))
             (aref 2d-matrix n_x (+ n_y 0))))
     (* d_time)))

(defun diff-of-vertical-and-horizontal (2d-matrix n_x n_y &optional (d_time 1))
  (/ (abs (- (aref 2d-matrix (+ n_x 1) (+ n_y 1))
             (aref 2d-matrix (+ n_x 0) (+ n_y 0))))
     (* d_time)))


(defun diff-spectrogram-of-spectrogram (spectrogram function-of-diff)
  ;;amplitude-diff-p freq-diff-p
  (let* ((len-time-windows (length spectrogram))
         (len-freq-windows (length (aref spectrogram 0)))
         (d-time 1.00) ;; todo: this is temporary valuue
         (spectrogram-2d (array-array-into-2d-matrix spectrogram))
         (diff-spectrogram-array
           (make-array (list (1- len-time-windows) (1- len-freq-windows))
                       :initial-element #C(0.0 0.0))))
    (loop for i-time from 0 to (- len-time-windows 1 1)
          do (loop for i-freq from 0 to (- len-freq-windows 1 1)
                   do (setf
                       (aref diff-spectrogram-array i-time i-freq)
                       (funcall function-of-diff spectrogram-2d i-time i-freq d-time))))
    (2d-matrix-into-array-array diff-spectrogram-array)))




;;; difference graph of spectrogram.


(defun draw-wave-file-diff-spectrogram (wav-file png-file window-size slide-size function-of-diff)
  ;; @> (draw-wave-file-diff-pitch-spectrogram (nth 1 *wav-file-name-list*) *spectrogram-filename* 64 512 #'diff-of-vertical-and-horizontal))
  ;; (one of #'diff-of-vertical-and-horizontal #'diff-of-vertical #'diff-of-horizontal)
  ;; is available for function-fo-diff
  (let ((wave-data nil)
        (spectrogram nil)
        (diff-spectrogram nil)
        (png-llwrittingled nil))
    ;;
    (format t "read wave wile into wave-data: ~A.~%" wav-file)
    (setf wave-data (read-wav-file-into-wave-data wav-file))
    ;;
    (format t "fold wave-data into spectrogram: ~A.~%" (format-wave-data-info wave-data))
    (setf spectrogram
          (spectrogram-of-series (wave-data-wave-series wave-data) window-size slide-size))
    ;;
    (format t "wave-data into pitch-diff matrix ~A.~%" ())
    (setf diff-spectrogram
          (diff-spectrogram-of-spectrogram spectrogram function-of-diff))
    ;;
    (format t "write diff-spectrogram into png image: ~A.~%" png-file)
    (setf png-llwrittingled (draw-spectrogram diff-spectrogram png-file))
    png-llwrittingled
    nil))


;; test
  
;; (plot-series (subseq (series-of-wav-file (nth 0 *wav-file-name-list*)) 0 10000))

;; (keyth :chunk-data (keys-of-wave-data))
